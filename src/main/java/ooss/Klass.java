package ooss;

import java.util.List;
import java.util.Objects;
import java.util.ArrayList;

public class Klass {
    private final int number;
    private Student klassLeader;

    private List<Person> klassMembers = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void assignLeader(Student student) {
        if (!student.isIn(this)) {
            System.out.println("It is not one of us.");
        } else {
            this.klassLeader = student;
            // Subtle difference between forEach and stream().forEach, stream() one can process in any order
            // See https://stackoverflow.com/questions/29090655/what-is-the-difference-between-foreach-and-stream-foreach
            this.klassMembers.stream().forEach(klassMember -> klassMember.sayUponLeaderAssignment(this.getNumber(), this.klassLeader.getName()));
        }
    }

    public boolean isLeader(Student student) {
        return student.equals(this.klassLeader);
    }

    public void attach(Person klassMember) {
        this.klassMembers.add(klassMember);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
