package ooss;

public class Student extends Person {
    private Klass studentKlass;
    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String studentDescription = "";
        if (studentKlass!=null) {
            if (studentKlass.isLeader(this)) {
                studentDescription = " I am the leader of class " + studentKlass.getNumber() + ".";
            } else {
                studentDescription = " I am in class " + studentKlass.getNumber() + ".";
            }
        }
        return super.introduce() + " I am a student." + studentDescription;
    }

    // Checks whether student is in a klass
    public boolean isIn(Klass klass) {
        return klass.equals(this.studentKlass);
    }

    // Allows the student to join a klass
    public void join(Klass klass) {
        this.studentKlass = klass;
    }

    @Override
    public void sayUponLeaderAssignment(Integer classNumber, String leaderName) {
        System.out.printf(String.format("I am %s, student of Class %d. I know %s become Leader.", this.getName(), classNumber, leaderName));
    }
}
