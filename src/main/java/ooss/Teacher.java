package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


public class Teacher extends Person {
    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    List<Klass> taughtKlasses = new ArrayList<>();

    @Override
    public String introduce() {
        String taughtklassesString = taughtKlasses.isEmpty() ? "" : " I teach Class " + taughtKlasses.stream().map(Klass::getNumber).map(Objects::toString).collect(Collectors.joining(", ")) + ".";
        return super.introduce() + " I am a teacher." + taughtklassesString;
    }

    public void assignTo(Klass klass) {
        this.taughtKlasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.taughtKlasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return taughtKlasses.stream().anyMatch(student::isIn);
    }

    @Override
    public void sayUponLeaderAssignment(Integer classNumber, String leaderName) {
        System.out.printf(String.format("I am %s, teacher of Class %d. I know %s become Leader.", this.getName(), classNumber, leaderName));
    }
}
